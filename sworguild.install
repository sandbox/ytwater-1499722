<?php

/**
 * @file
 * Sets up the base table for our entity and a table to store information about
 * the entity types.
 */


/**
 * Implements hook_install().
 */
function sworguild_install() {
  // Create swor AvatarType
  if (!avatar_get_types('swor')) {
    $swortype = new AvatarType();
    $swortype->label = "Star Wars Old Republic Character";
    $swortype->type = "swor";
    $swortype->module = "sworguild";
    avatar_type_save($swortype);
  }
  
  // Create automatic fields
  $fields = sworguild_install_fields();
  foreach ($fields as $field) {
    if (!field_info_field($field['field_name'])) {
      field_create_field($field);
    }
  }
  
  // attach fields to swor type
  $instances = sworguild_install_instances();
  foreach ($instances as $instance) {
    if (!field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
      field_create_instance($instance);
    }
  }
}

/**
 * Implements hook_uninstall().
 * Delete all avatars created by this module as well a fields and instances if possible.
 */
function sworguild_uninstall() {
  
  $query = db_select('avatar', 'a');
  $query->fields('a', array('aid'));
  $query->condition('type', 'swor');
  $aids = $query->execute()->fetchCol();
  avatar_delete_multiple($aids);
  
  $instances = field_info_instances('avatar', 'swor');
  foreach ($instances as $instance) {
    field_delete_instance($instance, TRUE);
  }
  
  $fields = sworguild_install_fields();
  foreach ($fields as $key => $value) {
    field_delete_field($key);
  }
}

/**
 * Define the fields our AvatarType uses in a function so we can install as well as uninstall them.
 */
function sworguild_install_fields() {
 return array (
  'field_level' => 
  array (
    'translatable' => '0',
    'entity_types' => 
    array (
    ),
    'settings' => 
    array (
    ),
    'storage' => 
    array (
      'type' => 'field_sql_storage',
      'settings' => 
      array (
      ),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => 
      array (
        'sql' => 
        array (
          'FIELD_LOAD_CURRENT' => 
          array (
            'field_data_field_level' => 
            array (
              'value' => 'field_level_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => 
          array (
            'field_revision_field_level' => 
            array (
              'value' => 'field_level_value',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => 
    array (
    ),
    'indexes' => 
    array (
    ),
    'field_name' => 'field_level',
    'type' => 'number_integer',
    'module' => 'number',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'int',
        'not null' => false,
      ),
    ),
    'bundles' => 
    array (
      'avatar' => 
      array (
        0 => 'swor',
      ),
    ),
  ),
  'field_gender' => 
  array (
    'translatable' => '0',
    'entity_types' => 
    array (
    ),
    'settings' => 
    array (
      'allowed_values' => 
      array (
        'male' => 'Male',
        'female' => 'Female',
      ),
      'allowed_values_function' => '',
    ),
    'storage' => 
    array (
      'type' => 'field_sql_storage',
      'settings' => 
      array (
      ),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => 
      array (
        'sql' => 
        array (
          'FIELD_LOAD_CURRENT' => 
          array (
            'field_data_field_gender' => 
            array (
              'value' => 'field_gender_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => 
          array (
            'field_revision_field_gender' => 
            array (
              'value' => 'field_gender_value',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => 
    array (
    ),
    'indexes' => 
    array (
      'value' => 
      array (
        0 => 'value',
      ),
    ),
    'field_name' => 'field_gender',
    'type' => 'list_text',
    'module' => 'list',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
      ),
    ),
    'bundles' => 
    array (
      'avatar' => 
      array (
        0 => 'swor',
      ),
    ),
  ),
   'field_faction' =>
   array (
     'translatable' => '0',
     'entity_types' =>
     array (
     ),
     'settings' =>
     array (
       'allowed_values' =>
       array (
         'good' => 'good',
         'bad' => 'bad',
       ),
       'allowed_values_function' => '',
     ),
     'storage' =>
     array (
       'type' => 'field_sql_storage',
       'settings' =>
       array (
       ),
       'module' => 'field_sql_storage',
       'active' => '1',
       'details' =>
       array (
         'sql' =>
         array (
           'FIELD_LOAD_CURRENT' =>
           array (
             'field_data_field_faction' =>
             array (
               'value' => 'field_faction_value',
             ),
           ),
           'FIELD_LOAD_REVISION' =>
           array (
             'field_revision_field_faction' =>
             array (
               'value' => 'field_faction_value',
             ),
           ),
         ),
       ),
     ),
     'foreign keys' =>
     array (
     ),
     'indexes' =>
     array (
       'value' =>
       array (
         0 => 'value',
       ),
     ),
     'field_name' => 'field_faction',
     'type' => 'list_text',
     'module' => 'list',
     'active' => '1',
     'locked' => '0',
     'cardinality' => '1',
     'deleted' => '0',
     'columns' =>
     array (
       'value' =>
       array (
         'type' => 'varchar',
         'length' => 255,
         'not null' => false,
       ),
     ),
     'bundles' =>
     array (
       'avatar' =>
       array (
         0 => 'swor',
       ),
     ),
   ),   
  'field_class' => 
  array (
    'translatable' => '0',
    'entity_types' => 
    array (
    ),
    'settings' => 
    array (
      'allowed_values' => 
      array (
        'class1' => 'class1',
        'class2' => 'class2',
      ),
      'allowed_values_function' => '',
    ),
    'storage' => 
    array (
      'type' => 'field_sql_storage',
      'settings' => 
      array (
      ),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => 
      array (
        'sql' => 
        array (
          'FIELD_LOAD_CURRENT' => 
          array (
            'field_data_field_class' => 
            array (
              'value' => 'field_class_value',
            ),
          ),
          'FIELD_LOAD_REVISION' => 
          array (
            'field_revision_field_class' => 
            array (
              'value' => 'field_class_value',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => 
    array (
    ),
    'indexes' => 
    array (
      'value' => 
      array (
        0 => 'value',
      ),
    ),
    'field_name' => 'field_class',
    'type' => 'list_text',
    'module' => 'list',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'varchar',
        'length' => 255,
        'not null' => false,
      ),
    ),
    'bundles' => 
    array (
      'avatar' => 
      array (
        0 => 'swor',
      ),
    ),
  ),
  'field_portrait' => 
  array (
    'translatable' => '0',
    'entity_types' => 
    array (
    ),
    'settings' => 
    array (
      'uri_scheme' => 'public',
      'default_image' => 0,
    ),
    'storage' => 
    array (
      'type' => 'field_sql_storage',
      'settings' => 
      array (
      ),
      'module' => 'field_sql_storage',
      'active' => '1',
      'details' => 
      array (
        'sql' => 
        array (
          'FIELD_LOAD_CURRENT' => 
          array (
            'field_data_field_portrait' => 
            array (
              'fid' => 'field_portrait_fid',
              'alt' => 'field_portrait_alt',
              'title' => 'field_portrait_title',
              'width' => 'field_portrait_width',
              'height' => 'field_portrait_height',
            ),
          ),
          'FIELD_LOAD_REVISION' => 
          array (
            'field_revision_field_portrait' => 
            array (
              'fid' => 'field_portrait_fid',
              'alt' => 'field_portrait_alt',
              'title' => 'field_portrait_title',
              'width' => 'field_portrait_width',
              'height' => 'field_portrait_height',
            ),
          ),
        ),
      ),
    ),
    'foreign keys' => 
    array (
      'fid' => 
      array (
        'table' => 'file_managed',
        'columns' => 
        array (
          'fid' => 'fid',
        ),
      ),
    ),
    'indexes' => 
    array (
      'fid' => 
      array (
        0 => 'fid',
      ),
    ),
    'field_name' => 'field_portrait',
    'type' => 'image',
    'module' => 'image',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    'deleted' => '0',
    'columns' => 
    array (
      'fid' => 
      array (
        'description' => 'The {file_managed}.fid being referenced in this field.',
        'type' => 'int',
        'not null' => false,
        'unsigned' => true,
      ),
      'alt' => 
      array (
        'description' => 'Alternative image text, for the image\'s \'alt\' attribute.',
        'type' => 'varchar',
        'length' => 512,
        'not null' => false,
      ),
      'title' => 
      array (
        'description' => 'Image title text, for the image\'s \'title\' attribute.',
        'type' => 'varchar',
        'length' => 1024,
        'not null' => false,
      ),
      'width' => 
      array (
        'description' => 'The width of the image in pixels.',
        'type' => 'int',
        'unsigned' => true,
      ),
      'height' => 
      array (
        'description' => 'The height of the image in pixels.',
        'type' => 'int',
        'unsigned' => true,
      ),
    ),
    'bundles' => 
    array (
      'avatar' => 
      array (
        0 => 'swor',
      ),
    ),
  ),
);
}

/**
 * Define the AvatarType instance in code so we can install and uninstall it.
 * 
 * TO EXPORT: var_export(field_info_instances('avatar', 'swor'));
 */
function sworguild_install_instances() {
  return 
array (
  'field_level' => 
  array (
    'label' => 'Level',
    'widget' => 
    array (
      'weight' => '1',
      'type' => 'number',
      'module' => 'number',
      'active' => 0,
      'settings' => 
      array (
      ),
    ),
    'settings' => 
    array (
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => false,
    ),
    'display' => 
    array (
      'default' => 
      array (
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => '0',
        'settings' => 
        array (
          'thousand_separator' => ' ',
          'decimal_separator' => '.',
          'scale' => 0,
          'prefix_suffix' => true,
        ),
        'module' => 'number',
      ),
      'teaser' => 
      array (
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => '0',
        'settings' => 
        array (
          'thousand_separator' => ' ',
          'decimal_separator' => '.',
          'scale' => 0,
          'prefix_suffix' => true,
        ),
        'module' => 'number',
      ),
      'popup' => 
      array (
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => '0',
        'settings' => 
        array (
          'thousand_separator' => ' ',
          'decimal_separator' => '.',
          'scale' => 0,
          'prefix_suffix' => true,
        ),
        'module' => 'number',
      ),
      'select_block' => 
      array (
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => '0',
        'settings' => 
        array (
          'thousand_separator' => ' ',
          'decimal_separator' => '.',
          'scale' => 0,
          'prefix_suffix' => true,
        ),
        'module' => 'number',
      ),
      'select_block_pulldown' => 
      array (
        'label' => 'inline',
        'type' => 'number_integer',
        'weight' => '0',
        'settings' => 
        array (
          'thousand_separator' => ' ',
          'decimal_separator' => '.',
          'scale' => 0,
          'prefix_suffix' => true,
        ),
        'module' => 'number',
      ),
    ),
    'required' => 1,
    'description' => 'This is the level your character is in game.',
    'default_value' => 
    array (
      0 => 
      array (
        'value' => '1',
      ),
    ),
    'field_name' => 'field_level',
    'entity_type' => 'avatar',
    'bundle' => 'swor',
    'deleted' => '0',
  ),
  'field_gender' => 
  array (
    'label' => 'Gender',
    'widget' => 
    array (
      'weight' => '2',
      'type' => 'options_select',
      'module' => 'options',
      'active' => 1,
      'settings' => 
      array (
      ),
    ),
    'settings' => 
    array (
      'user_register_form' => false,
    ),
    'display' => 
    array (
      'default' => 
      array (
        'label' => 'above',
        'type' => 'list_default',
        'weight' => '1',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'teaser' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'popup' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'select_block' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'select_block_pulldown' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
    ),
    'required' => 1,
    'description' => 'The gender of your character.',
    'default_value' => NULL,
    'id' => '8',
    'field_id' => '6',
    'field_name' => 'field_gender',
    'entity_type' => 'avatar',
    'bundle' => 'swor',
    'deleted' => '0',
  ),
  'field_faction' =>
  array (
    'label' => 'Faction',
    'widget' =>
    array (
      'weight' => '2',
      'type' => 'options_select',
      'module' => 'options',
      'active' => 1,
      'settings' =>
      array (
      ),
    ),
    'settings' =>
    array (
      'user_register_form' => false,
    ),
    'display' =>
    array (
      'default' =>
      array (
        'label' => 'above',
        'type' => 'list_default',
        'weight' => '1',
        'settings' =>
        array (
        ),
        'module' => 'list',
      ),
      'teaser' =>
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' =>
        array (
        ),
        'module' => 'list',
      ),
      'popup' =>
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' =>
        array (
        ),
        'module' => 'list',
      ),
      'select_block' =>
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' =>
        array (
        ),
        'module' => 'list',
      ),
      'select_block_pulldown' =>
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' =>
        array (
        ),
        'module' => 'list',
      ),
    ),
    'required' => 1,
    'description' => 'The faction of your character.',
    'default_value' => NULL,
    'field_name' => 'field_faction',
    'entity_type' => 'avatar',
    'bundle' => 'swor',
    'deleted' => '0',
  ),
  'field_class' => 
  array (
    'label' => 'Class',
    'widget' => 
    array (
      'weight' => '3',
      'type' => 'options_buttons',
      'module' => 'options',
      'active' => 1,
      'settings' => 
      array (
      ),
    ),
    'settings' => 
    array (
      'user_register_form' => false,
    ),
    'display' => 
    array (
      'default' => 
      array (
        'label' => 'above',
        'type' => 'list_default',
        'weight' => '2',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'teaser' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'popup' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'select_block' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
      'select_block_pulldown' => 
      array (
        'label' => 'hidden',
        'type' => 'list_default',
        'weight' => '0',
        'settings' => 
        array (
        ),
        'module' => 'list',
      ),
    ),
    'required' => 1,
    'description' => 'The class your character is.',
    'default_value' => NULL,
    'field_name' => 'field_class',
    'entity_type' => 'avatar',
    'bundle' => 'swor',
    'deleted' => '0',
  ),
  'field_portrait' => 
  array (
    'label' => 'Portrait',
    'widget' => 
    array (
      'weight' => '4',
      'type' => 'image_image',
      'module' => 'image',
      'active' => 1,
      'settings' => 
      array (
        'progress_indicator' => 'throbber',
        'preview_image_style' => 'thumbnail',
      ),
    ),
    'settings' => 
    array (
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'alt_field' => 0,
      'title_field' => 0,
      'user_register_form' => false,
    ),
    'display' => 
    array (
      'default' => 
      array (
        'label' => 'hidden',
        'type' => 'image',
        'weight' => '3',
        'settings' => 
        array (
          'image_style' => 'meduim',
        ),
      ),
      'teaser' => 
      array (
        'label' => 'above',
        'type' => 'hidden',
        'weight' => '0',
        'settings' => 
        array (
        ),
      ),
      'popup' => 
      array (
        'label' => 'above',
        'type' => 'hidden',
        'weight' => '0',
        'settings' => 
        array (
        ),
      ),
      'select_block' => 
      array (
        'label' => 'above',
        'type' => 'hidden',
        'weight' => '0',
        'settings' => 
        array (
        ),
      ),
      'select_block_pulldown' => 
      array (
        'label' => 'above',
        'type' => 'hidden',
        'weight' => '0',
        'settings' => 
        array (
        ),
      ),
    ),
    'required' => 0,
    'description' => 'Upload a picture of your characters portrait, or leave blank to use your default class.',
    'field_name' => 'field_portrait',
    'entity_type' => 'avatar',
    'bundle' => 'swor',
    'deleted' => '0',
  ),
);
}